package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class DemoCOS_StepDefinition {
	
	WebDriver driver;
	Actions builder;
	WebDriverWait wait;
	
	@Given("^je suis sur la page du gassi de dev$")
	public void je_suis_sur_la_page_du_gassi_de_dev(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\A758909\\Eclipse_Workspaces\\Cucumber\\FreeBDDFramework - Copie\\drivers\\chromedriver_win32_79\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver,30);
		driver.get("https://gassi.si.francetelecom.fr/");
		driver.manage().window().maximize();
	}
	
	@Then("^je rentre mes identifiants$")
	public void je_rentre_mes_identifiants() {
		driver.switchTo().frame("bgmainframe");
		driver.findElement(By.name("USER")).sendKeys("LGCX2586");
		driver.findElement(By.name("PASSWORD")).sendKeys("lgcx2586");
		driver.findElement(By.id("spanLinkValidForm")).click();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	@Then("^j'entre dans le portail de mes applis$")
	public void j_entre_dans_le_portail_de_mes_applis() {
		
		//String windowGassi = driver.getWindowHandle();
		//System.out.println(windowGassi);
		driver.switchTo().window("Mon SI : l'accueil dans le Système d'Information");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("bgmainframe")));
		//driver.switchTo().frame("bgmainframe");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div/div[2]/div[3]/table/tbody/tr[3]/td[2]/a")));
		driver.findElement(By.xpath("/html/body/div/div/div[2]/div[3]/table/tbody/tr[3]/td[2]/a")).click();
	}

}
