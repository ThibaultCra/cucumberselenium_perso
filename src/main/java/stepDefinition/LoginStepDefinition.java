package stepDefinition;

import static org.junit.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginStepDefinition {
	
	WebDriver driver;
	Actions builder;

	@Given("^User is already on Login Page$")
	public void user_already_on_login_page() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\A758909\\Eclipse_Workspaces\\Cucumber\\FreeBDDFramework - Copie\\drivers\\chromedriver_win32_79\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
	}
	
	@When("^title of login page is Free CRM$")
	public void title_of_login_page_is_free_CRM() {
		String title = driver.getTitle();
		System.out.println(title);
		assertEquals("OrangeHRM", title);
	}
	
	@Then("^user enters username and user enters password$")
	public void user_enters_username_and_user_enters_password() {
		
		driver.findElement(By.name("txtUsername")).sendKeys("Admin");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		WebElement password = driver.findElement(By.id("txtPassword"));
		password.sendKeys("admin123");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@And("^user clicks on login button$")
	public void user_clicks_on_login_button() {
		driver.findElement(By.id("btnLogin")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@And("^user is on home page$")
	public void user_is_on_home_page() {
		String title = driver.getTitle();
		System.out.println(title);
		assertEquals("OrangeHRM", title);
	}
	
	@And("^user goes to admin job job categories$")
	public void user_goes_to_admin_job_job_categories() throws InterruptedException {
		Actions builder = new Actions(driver);
		WebElement adminPanel = driver.findElement(By.id("menu_admin_viewAdminModule"));
		WebElement jobPanel = driver.findElement(By.id("menu_admin_Job"));
		builder.moveToElement(adminPanel).moveToElement(jobPanel).build().perform();
		
		for(int i=0;i<=2;i++)
		{
			builder.sendKeys(Keys.DOWN).build().perform();
			Thread.sleep(1000);
		}
		driver.findElement(By.id("menu_admin_jobCategory")).click();
		Thread.sleep(2000);
	}
	
	@And("^user enters a new job category$")
	public void user_enters_a_new_job_category() throws InterruptedException {
		driver.findElement(By.name("btnAdd")).click();
		Thread.sleep(500);
		driver.findElement(By.id("jobCategory_name")).sendKeys("Fermenteur rotatif");
		Thread.sleep(500);
		driver.findElement(By.id("btnSave")).click();
		Thread.sleep(500);
	}
	
	@And("^user logs out$")
	public void user_logs_out() {
		WebElement linkWelcome = driver.findElement(By.id("welcome"));
		linkWelcome.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		WebElement linkLogout = driver.findElement(By.xpath("//*[@id=\"welcome-menu\"]/ul/li[2]/a"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", linkLogout);
	}
	
	@And("^user leaves the browser$")
	public void user_leaves_the_browser() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.close();
	}
	
}
